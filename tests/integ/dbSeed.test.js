import { describe, it, expect } from "vitest";
import mongoose from "mongoose";
import Recipe from "../../src/recipes/model.js";

describe("db", () => {
    it("works", async () => {
        await mongoose.connect(process.env.MONGODB_URI, {
        });
        let recipes = await Recipe.find()
        if (recipes.length === 0) {
            let recipe = new Recipe({
                name: "bolognaise",
                description: "a classic bolognese dish",
                ingredients: ["pasta", "tomatoes", "onions", "olives", "garlic", "butter", "salt", "pepper"],
                instructions: ["boil water", "add pasta", "add sauce", "serve"]

            })

            await recipe.save()
        }
        recipes = await Recipe.find()
        expect(recipes.length).toBe(1)
    })
});