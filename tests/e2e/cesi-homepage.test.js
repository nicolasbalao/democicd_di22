import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://www.cesi.fr/');
  //await page.getByRole('button', { name: 'Continuer sans accepter →' }).click();
  await page.getByPlaceholder('Votre recherche : mastère,').click();
  await page.getByPlaceholder('Votre recherche : mastère,').fill('développeur informatique');
  await page.getByRole('button', { name: 'Chercher' }).click();
  await page.getByRole('link', { name: 'Développeur·se informatique', exact: true }).click();
});