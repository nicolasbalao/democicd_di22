FROM node:22 as builder

ADD package.json /app/package.json

WORKDIR /app
RUN npm install
ADD . /app

EXPOSE 3000

CMD ["npm", "start"]